@servers(['hml' => 'arthurfaria@localhost -p 52800'])

@setup
    $repository = 'git@gitlab.com:arthurgomesfaria/laravelcompleto.git';
@endsetup

@story('rollback')
    clone_repository
  	rollback
@endstory

@story('deploy')
    clone_repository
    update
@endstory

@task('rollback')
    kubectl rollout undo deployment travel-deployment
@endtask

@task('update')
  	kubectl apply -f travel-deployment.yml
@endtask

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
@endtask